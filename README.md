[![Crates.io](https://img.shields.io/crates/v/with_tempdir)](https://crates.io/crates/with_tempdir)
[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/woshilapin/with_tempdir)](https://gitlab.com/woshilapin/with_tempdir/pipelines)
[![License GPL-3.0+](https://img.shields.io/badge/license-GPL--3.0%2B-blue)](https://www.gnu.org/licenses/gpl-3.0.en.html)

Tempdir injection for tests
===========================
This small project is providing you a procedural macro to inject a temporary
directory in your test.

```rust
#[with_tempdir]
#[test]
fn my_test(path: &Path) {
  // do stuff in folder `path`
}
```

Look at the [documentation](https://woshilapin.gitlab.io/with_tempdir/) for more.
